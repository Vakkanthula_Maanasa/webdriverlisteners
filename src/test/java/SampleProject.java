import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class SampleProject{
    public WebDriver driver;
    EventFiringWebDriver eventFiringWebDriver;
    understandingListenerActions understandingListenerActions =new understandingListenerActions();

    @Test
    public void understandingWebDriverListeners() throws InterruptedException, IOException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
         eventFiringWebDriver=new EventFiringWebDriver(driver);
        MyTestListener myTestListener =new MyTestListener();
        eventFiringWebDriver.register(myTestListener);
        eventFiringWebDriver.manage().window().maximize();
        eventFiringWebDriver.get("https://www.demoblaze.com/");
        eventFiringWebDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        understandingListenerActions.fillSigUpForm(eventFiringWebDriver);
        eventFiringWebDriver.quit();
    }

}
