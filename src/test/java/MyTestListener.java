import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

import java.io.File;
import java.io.IOException;

public class MyTestListener extends AbstractWebDriverEventListener {
    understandingListenerActions understandingListenerActions =new understandingListenerActions();
    @Override
    public void beforeAlertAccept(WebDriver driver) {
        System.out.println("Before accepting the alert window with a message : "+driver.switchTo().alert().getText());
    }

    @Override
    public void afterAlertAccept(WebDriver driver) {
        System.out.println("After accepting the alert window ");

    }

    @Override
    public void beforeNavigateTo(String url, WebDriver driver)
    {
        System.out.println("Before Opening "+url);
    }

    @Override
    public void afterNavigateTo(String url, WebDriver driver) {
        System.out.println("After Opening "+url+" in browser and opening"+driver.getTitle());
    }

    @Override
    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
        System.out.println("Before Finding " +driver.findElement(by));
    }

    @Override
    public void afterFindBy(By by, WebElement element, WebDriver driver) {
        System.out.println("After Finding "+element.getAttribute("id"));
    }

    @Override
    public void beforeClickOn(WebElement element, WebDriver driver) {
        System.out.println("beforeClicking on " +element.getText()+" Button");
    }

    @Override
    public void afterClickOn(WebElement element, WebDriver driver) {
        System.out.println("afterClicking on " +element.getText()+" Button");
    }

    @Override
    public void beforeChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
        System.out.println("Entering values in the empty fields");
    }

    @Override
    public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
        System.out.println("Entering "+ element.getAttribute("value"));
    }

    @Override
    public void afterNavigateRefresh(WebDriver driver) {
        System.out.println("After Refreshing");
    }

    @Override
    public void onException(Throwable throwable, WebDriver driver) {
        File file =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(file,new File("/Users/vakkanthulamaanasa/Documents/Setup/Assignment/src/main/ScreenShot.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
