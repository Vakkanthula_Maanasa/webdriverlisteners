import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;

public class understandingListenerActions extends understandingListenerobjects
{
    EventFiringWebDriver eventFiringWebDriver;
    public void fillSigUpForm(EventFiringWebDriver eventFiringWebDriver)
    {
        eventFiringWebDriver.findElement(clickOnSignUp).click();
        WebDriverWait wait = new WebDriverWait(eventFiringWebDriver,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(username));
        WebElement element = eventFiringWebDriver.findElement(username);
        element.sendKeys(usernameToEnter);
        WebElement element1 =eventFiringWebDriver.findElement(password);
        element1.sendKeys(passwordToEnter);
        eventFiringWebDriver.findElement(submit).click();
        wait.until(ExpectedConditions.alertIsPresent());
        eventFiringWebDriver.switchTo().alert().accept();
        eventFiringWebDriver.navigate().refresh();
    }

}
